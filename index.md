---
layout: default
---

![](/assets/img/logo.png)

# covid19 data api

This project provides an API for the corona data of Germany until 2023-06-01  
(as the used data sources no longer provide any new data).

The documentation of the API can be found [here (swagger ui)](/docs).

The sources of all components can be found in the Gitlab group [smsvc/c19d](https://gitlab.com/smsvc/c19d).

