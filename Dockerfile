FROM jekyll/jekyll

WORKDIR /landingpage
COPY . /landingpage
RUN chown -R jekyll /landingpage

RUN bundle install
CMD ["/usr/local/bin/bundle", "exec", "jekyll", "serve", "--host", "0.0.0.0"]
