# COVID-19 Data Landing Page

This project provides the landing page for c19d

## Usage

```
gem install bundler jekyll
bundle install
bundle exec jekyll serve
```

```
docker build -t landingpage .
docker run -p 4000:4000 landingpage
```
